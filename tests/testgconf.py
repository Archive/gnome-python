import unittest
import common
import tempfile
from common import gconf

class GConfTest(unittest.TestCase):
    def testBug595566(self):
        c = gconf.Client()
        self.assertRaises(TypeError, c.set_list, '/test/mec', gconf.VALUE_STRING, [None])


if __name__ == '__main__':
    unittest.main()
